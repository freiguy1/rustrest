use iron::{ BeforeMiddleware, typemap };
use rustc_serialize::json::Json;
use iron::prelude::*;
use config::Config;

use rustorm::pool::ManagedPool;
use std::sync::{ Mutex, Arc };

//use mysql::conn::pool::MyPool;
//use mysql::conn::MyOpts;

pub struct Pool {
    pool: Arc<Mutex<ManagedPool>>
}

impl Pool {
    pub fn new(config: &Json) -> Result<Pool, &'static str> {
        let conn_string = match config.fetch_string(&["database"]) {
            Ok(c) => c,
            _ => return Err("database connection string not available")
        };
        let pool = match ManagedPool::init(&conn_string, 16) {
            Ok(p) => p,
            _ => return Err("error creating database connection pool")
        };
        Ok(Pool {
            pool: Arc::new(Mutex::new(pool))
        })
    }
}

impl typemap::Key for Pool { type Value = Arc<Mutex<ManagedPool>>; }

impl BeforeMiddleware for Pool {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions.insert::<Pool>(self.pool.clone());
        Ok(())
    }
}

// ---------------- MYSQL STUFF -----------------

/*
pub struct Pool {
    pool: MyPool
}

impl Pool {
    pub fn new(config: &Json) -> Result<Pool, &'static str> {
        let user = match config.fetch_string(&["mysql", "username"]) {
            Ok(u) => u,
            Err(_) => return Err("mysql object and/or user property missing in config")
        };
        let password = match config.fetch_string(&["mysql", "password"]) {
            Ok(p) => p,
            Err(_) => return Err("mysql object and/or password property missing in config")
        };
        let addr = match config.fetch_string(&["mysql", "addr"]) {
            Ok(a) => a,
            Err(_) => return Err("mysql object and/or addr property missing in config")
        };
        let database = match config.fetch_string(&["mysql", "database"]) {
            Ok(d) => d,
            Err(_) => return Err("mysql object and/or database property missing in config")
        };
        let port = match config.fetch_u64(&["mysql", "port"]) {
            Ok(p) => p,
            Err(_) => return Err("mysql object and/or port property missing or invalid in config")
        };

        let opts = MyOpts {
            user: Some(user.to_string()),
            pass: Some(password.to_string()),
            db_name: Some(database.to_string()),
            tcp_addr: Some(addr.to_string()),
            tcp_port: port as u16,
            ..Default::default()
        };

        let pool = match MyPool::new(opts) {
            Ok(p) => p,
            Err(_) => return Err("could not initialize db connection pool with given parameters")
        };

        Ok(Pool {
            pool: pool
        })
    }
}

impl typemap::Key for Pool { type Value = MyPool; }

impl BeforeMiddleware for Pool {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions.insert::<Pool>(self.pool.clone());
        Ok(())
    }
}
*/
