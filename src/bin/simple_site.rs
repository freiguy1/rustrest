extern crate router;
extern crate iron;
extern crate rustrest;
extern crate rustc_serialize;
//extern crate mysql;
extern crate rustorm;

use router::Router;
use iron::prelude::*;

fn main() {
    let mut router = Router::new();
    router.get("/", |_: &mut Request| Ok(Response::with("Welcome to the API")));
    router.get("/todos/", rustrest::handlers::unsecured::RespHandler::new(actions::GetTodos));

    rustrest::initialize_router(&mut router);

    let mut chain = Chain::new(router);
    rustrest::initialize_chain(&mut chain);

    Iron::new(chain)
        .http("0.0.0.0:6767").unwrap();
}

mod actions {
    use rustrest::handlers::unsecured::{ RespAction, Bundle };
    use rustrest::handlers::{ ActionError };
    use std::collections::HashMap;


    #[derive(RustcEncodable)]
    pub struct Todo {
        done: bool,
        description: &'static str 
    }

    pub struct GetTodos;

    impl RespAction for GetTodos {
        type TResp = Vec<Todo>;

        // Step 1
        fn validate(&self,
                    _: &::router::Params,
                    _: &HashMap<String, Vec<String>>) -> HashMap<&str, &str> {
            HashMap::new()
        }


        // Step 2
        fn handle(&self,
                  bundle: Bundle) -> Result<Self::TResp, ActionError> {
            let db;
            {
                let pool = bundle.pool.lock().unwrap();
                db = pool.connect().unwrap();
            }
            let abbreviations = ::rustorm::query::Query::select().from_table("state").column("abbreviation").retrieve(db.as_ref()).unwrap();
            //let abbreviations: Vec<String> = abbreviations.dao.iter().map(|dao| dao.get::<String>("abbreviation")).collect();
            println!("abbrev: {:?}", abbreviations.dao);

            Ok(vec![
               Todo { done: true, description: "clean dishes" },
               Todo { done: false, description: "make food" },
               Todo { done: false, description: "eat food" },
               Todo { done: true, description: "profit" }])
        }
    }
}

