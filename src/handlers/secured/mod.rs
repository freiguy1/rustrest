use rustc_serialize::Decodable;
use std::any::Any;
use iron::prelude::*;
use iron::status;
use std::collections::HashMap;
use std::sync::{ Arc, Mutex };

mod req;
mod req_resp;
mod resp;

pub use self::req::{ ReqAction, ReqHandler };
pub use self::req_resp::{ ReqRespAction, ReqRespHandler };
pub use self::resp::{ RespAction, RespHandler };

pub struct BundleWithReq<'a, TReq: Decodable + Any + Clone> {
    pub transfer_object: TReq,
    pub params: &'a ::router::Params,
    pub query_string: HashMap<String, Vec<String>>,
    pub current_user: &'a ::auth::CurrentUser,
    pub config: &'a ::std::sync::Arc<::rustc_serialize::json::Json>,
    pub pool: &'a Arc<Mutex<::rustorm::pool::ManagedPool>>
}

pub struct Bundle<'a> {
    pub params: &'a ::router::Params,
    pub query_string: HashMap<String, Vec<String>>,
    pub current_user: &'a ::auth::CurrentUser,
    pub config: &'a ::std::sync::Arc<::rustc_serialize::json::Json>,
    pub pool: &'a Arc<Mutex<::rustorm::pool::ManagedPool>>
}

fn extract_current_user<'a>(req: &'a Request) -> Result<&'a ::auth::CurrentUser, IronResult<Response>> {
    match req.extensions.get::<::auth::CurrentUser>().map(|current_user| current_user) {
        Some(user) => Ok(user),
        None => Err(Ok(Response::with(status::Unauthorized)))
    }
}
