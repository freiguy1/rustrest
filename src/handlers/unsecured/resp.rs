use handlers::{
    create_invalid_request_response,
    get_query_string,
    ActionError
};
use handlers::unsecured::{ Bundle, extract_current_user };
use iron::{ Handler, status };
use iron::prelude::*;
use iron::mime::Mime;
use router::Router;
use rustc_serialize::json::encode as json_encode;
use rustc_serialize::Encodable;
use std::collections::HashMap;
use std::any::Any;

pub trait RespAction: Send + Sync + Any {
    type TResp: 'static +  Encodable;

    // Step 1
    #[allow(unused_variables)]
    fn validate(&self,
                params: &::router::Params,
                query_string: &HashMap<String, Vec<String>>) -> HashMap<&str, &str> {
        HashMap::new()
    }

    // Step 2
    fn handle(&self,
              bundle: Bundle) -> Result<Self::TResp, ActionError>;
}

pub struct RespHandler<A: RespAction> {
    action: A,
}

impl<A: RespAction> RespHandler<A> {
    pub fn new(action: A) -> RespHandler<A> {
        RespHandler {
            action: action
        }
    }
}

impl<A: 'static + RespAction> Handler for RespHandler<A> {
    fn handle(&self, req: &mut Request) -> IronResult<Response> {
        // Get query string
        let query_string = match get_query_string(req) {
            Ok(q) => q,
            Err(e) => return e
        };

        // Get params
        let params = req.extensions.get::<Router>().unwrap();

        // Get database pool
        let pool = req.extensions.get::<::db::Pool>().unwrap();

        // Get configuration
        let config = req.extensions.get::<::config::ConfigManager>().unwrap();

        // Get user
        let current_user_opt = extract_current_user(req);

        // Validate
        let errors = self.action.validate(params, &query_string);
        if let Err(err_response) = create_invalid_request_response(errors) {
            return err_response;
        }

        let bundle = Bundle {
            params: params,
            query_string: query_string,
            current_user: current_user_opt,
            config: config,
            pool: pool
        };

        let response_body = try!(self.action.handle(bundle));
        let encoded_response_body = json_encode(&response_body).unwrap();
        let content_type = "application/json".parse::<Mime>().unwrap();
        Ok(Response::with((content_type, status::Ok, encoded_response_body)))
    }
}
