use rustc_serialize::Decodable;
use iron::prelude::*;
use iron::status;
use std::collections::HashMap;
use std::any::Any;
use urlencoded::{ UrlDecodingError, UrlEncodedQuery };

pub mod secured;
pub mod unsecured;

fn parse_request_from_json<TReq: 'static + Decodable + Any + Clone>(req: &mut Request) -> Result<TReq, IronResult<Response>> {
    match req.get::<::bodyparser::Struct<TReq>>() {
        Ok(Some(struct_body)) => Ok(struct_body),
        Ok(None) => Err(Ok(Response::with(status::BadRequest))),
        Err(err) => Err(Ok(Response::with((status::BadRequest, format!("{:?}", err)))))
    }
}

fn create_invalid_request_response(errors: HashMap<&str, &str>) -> Result<(), IronResult<Response>> {
    if errors.len() > 0 {
        Err(Ok(Response::with((status::BadRequest, format!("{:?}", errors)))))
    } else {
        Ok(())
    }
}

fn authorize_current_user_opt<A>(current_user_opt: &Option<&::auth::CurrentUser>, auth_fn: A) -> Result<(), IronResult<Response>>
    where A: Fn(&::auth::CurrentUser) -> bool {
    match *current_user_opt {
        Some(ref current_user) => {
            if auth_fn(current_user) { Ok(()) }
            else { Err(Ok(Response::with(status::Unauthorized))) }
        }
        None => Err(Ok(Response::with(status::Unauthorized))),
    }
}

fn get_query_string(req: &mut Request) -> Result<HashMap<String, Vec<String>>, IronResult<Response>> {
    match req.get::<UrlEncodedQuery>() {
        Ok(q) => Ok(q),
        Err(UrlDecodingError::EmptyQuery) => Ok(HashMap::new()),
        Err(_) => Err(Ok(Response::with(status::BadRequest)))
    }
}

#[derive(Debug)]
pub enum ActionError {
    Unauthorized,
    DatabaseError,
    NotFound
}

impl ::std::fmt::Display for ActionError {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Debug::fmt(self, f)
    }
}

impl ::std::error::Error for ActionError {
    fn description(&self) -> &str {
        match *self {
            ActionError::Unauthorized => "Unauthorized",
            ActionError::DatabaseError => "Database Error",
            ActionError::NotFound => "Item not found"
        }
    }
}


impl From<ActionError> for IronError {
    fn from(e: ActionError) -> IronError {
        match e {
            ActionError::Unauthorized => IronError::new(e, status::Unauthorized),
            ActionError::DatabaseError => IronError::new(e, status::InternalServerError),
            ActionError::NotFound => IronError::new(e, status::NotFound)
        }
    }
}
