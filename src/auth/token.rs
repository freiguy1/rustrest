use std::ops::Add;

use std::default::Default;
use crypto::sha2::Sha256;
use jwt::{ Token, Header, Registered };
use jwt::Token as JwtToken;

use iron::{ BeforeMiddleware, typemap };
use iron::prelude::*;

static EXPIRATION_HOURS: i64 = 7 * 24;

#[derive(Clone)]
pub struct TokenManager {
    app_secret: String
}

impl TokenManager {
    pub fn new(app_secret: String) -> TokenManager {
        TokenManager {
            app_secret: app_secret
        }
    }

    pub fn encode(&self, user_id: usize, domain: String) -> String {
        let header: Header = Default::default();
        let current_time = ::time::now_utc();
        let current_time_seconds = current_time.to_timespec().sec;
        let expiration_time = current_time.add(::time::Duration::hours(EXPIRATION_HOURS));
        let expiration_seconds = expiration_time.to_timespec().sec;
        let claims = Registered {
            iss: Some(domain),
            sub: Some(user_id.to_string()),
            exp: Some(expiration_seconds as u64),
            iat: Some(current_time_seconds as u64),
            ..Default::default()
        };
        let token = JwtToken::new(header, claims);

        token.signed(self.app_secret.as_ref(), Sha256::new()).unwrap()
    }

    pub fn decode(&self, encoded: &String) -> Option<usize> {
        let token = match Token::<Header, Registered>::parse(encoded.as_ref()) {
            Ok(t) => t,
            Err(_) => return None
        };

        if token.verify(self.app_secret.as_ref(), Sha256::new()) {
            let current_time_seconds = ::time::now_utc().to_timespec().sec;
            if current_time_seconds as u64 > token.claims.exp.unwrap() {
                None
            } else {
                Some(token.claims.sub.unwrap().parse().unwrap())
            }
        } else {
            None
        }
    }
}

impl typemap::Key for TokenManager { type Value = TokenManager; }

impl BeforeMiddleware for TokenManager {
    fn before(&self, req: &mut Request) -> IronResult<()> {
        req.extensions.insert::<TokenManager>(self.clone());
        Ok(())
    }
}
