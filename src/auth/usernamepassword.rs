
use router::Router;
use iron::prelude::*;
use iron::status;

pub fn initialize_router(router: &mut Router) {
    router.post("/auth/usernamepassword/create", create_user);
    router.post("/auth/usernamepassword/login", login);
}

#[derive(RustcDecodable, Debug, Clone)]
struct CreateUser {
    email: String,
    password: String,
    first_name: String,
    last_name: String
}

fn create_user(req: &mut Request) -> IronResult<Response> {
    let struct_body = req.get::<::bodyparser::Struct<CreateUser>>();
    match struct_body {
        Ok(Some(struct_body)) => println!("Parsed body:\n{:?}", struct_body),
        Ok(None) => println!("No body"),
        Err(err) => println!("Error: {:?}", err)
    }
    Ok(Response::with(status::Ok))
}

#[derive(RustcDecodable, Debug, Clone)]
struct LoginCredentials {
    email: String,
    password: String
}


fn login(req: &mut Request) -> IronResult<Response> {
    let struct_body = req.get::<::bodyparser::Struct<LoginCredentials>>();
    match struct_body {
        Ok(Some(struct_body)) => {
            println!("Parsed body:\n{:?}", struct_body);
            if let Some(user_id) = find_user_id_with_email_password(&struct_body.email, &struct_body.password) {
                Ok(::auth::auth_success_token_response(req, user_id))
            } else {
                Ok(Response::with(status::Unauthorized))
            }
        }
        Ok(None) => Ok(Response::with(status::BadRequest)),
        Err(err) => {
            println!("Error: {:?}", err);
            Ok(Response::with(status::BadRequest))
        }
    }
}

#[allow(unused_variables)]
fn find_user_id_with_email_password(email: &String, password: &String) -> Option<usize> {
    if password == "password" {
        Some(1)
    } else {
        None
    }
}

