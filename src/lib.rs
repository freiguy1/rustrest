extern crate hyper;
extern crate iron;
extern crate router;
extern crate bodyparser;
extern crate persistent;
extern crate url;
extern crate urlencoded;
extern crate rustc_serialize;
extern crate jwt;
extern crate crypto;
extern crate time;
extern crate mysql;
extern crate rustorm;


pub mod auth;
pub mod config;
pub mod handlers;
pub mod db;


use iron::middleware::Chain;
use router::Router;
use persistent::Read;

pub fn initialize_router(router: &mut Router) {
    auth::initialize_router(router);
}

pub fn initialize_chain(chain: &mut Chain) {
    // Initialize Mounts
    // let mut mount = Mount::new();
    // mount.mount("/", handler);
    // mount.mount("/v0.01", v0_01::initialize_router());
    // mount.mount("/auth", auth::initialize_router());

    // Initialize Chain
    // let mut chain = Chain::new(mount);
    let config_manager = config::ConfigManager::new();
    let config = config_manager.settings.clone();
    chain.link_before(config_manager);
    let db_pool = match db::Pool::new(&config) {
        Ok(p) => p,
        Err(e) => panic!(e)
    };
    chain.link_before(db_pool);
    let secret = config.find("secret").unwrap().as_string().unwrap();
    chain.link_before(auth::token::TokenManager::new(secret.to_string()));
    chain.link_before(auth::CurrentUserExtractor);
    chain.link_before(Read::<bodyparser::MaxBodyLength>::one(10*1024*1024));
}

